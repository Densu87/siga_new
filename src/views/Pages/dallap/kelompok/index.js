import KelompokBKB from './kelompok_bkb';
import RegisterBKB from './register_bkb';
import KelompokBKL from './kelompok_bkl';
import RegisterBKL from './register_bkl';
import KelompokUPPKS from './kelompok_uppks';
import RegisterUPPKS from './register_uppks';
import KelompokPIK from './kelompok_pik';
import RegisterPIK from './register_pik';

export {
    KelompokBKB, RegisterBKB, KelompokBKL, RegisterBKL, KelompokUPPKS, RegisterUPPKS, KelompokPIK, RegisterPIK
}